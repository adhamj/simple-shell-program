#format is target-name: target dependencies
#{-tab-}actions

# All Targets
all: Myshell

# Tool invocations
Myshell: Myshell.o line_parser.o
	gcc -g -Wall -o Myshell Myshell.o line_parser.o

# Depends on the source and header files

line_parser.o: line_parser.c line_parser.h
	gcc -g -Wall -c -o line_parser.o line_parser.c

Myshell.o: Myshell.c 
	gcc -g -Wall -c -o Myshell.o Myshell.c



#tell make that "clean" is not a file name!
.PHONY: clean

#Clean the build directory
clean: 
	rm -f *.o Myshell

 
