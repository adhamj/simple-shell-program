#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "line_parser.h"
#define STDIN 0
#define STDOUT 1
#define READ_END 0
#define WRITE_END 1

int execute_wraper(cmd_line* line);
int simple_strcmp(const char *str1, const char *str2);
int execute_regular(cmd_line *line);
int execute_with_pipe(cmd_line* line);

char path_name[PATH_MAX];
char line_input[PATH_MAX];

int main(int argc , char** argv){
    
    
    while(getcwd(path_name,PATH_MAX)){
 
        printf("%s>>>>",path_name);
        fgets(line_input,PATH_MAX,stdin);
        if( simple_strcmp("quit\n",line_input) == 0  ) return 0;
        
        cmd_line* command = parse_cmd_lines( line_input );
        
        if(command){
            if(execute_wraper(command)) {
                return -1;
            }
        }
        
        free_cmd_lines(command);
    }
    return 0;
}


int execute_wraper(cmd_line* line){
        
    if(line->next){
       return execute_with_pipe(line);
    }
    return execute_regular(line);
    

    
}


int execute_with_pipe(cmd_line* line){
    cmd_line* other_command = line->next;
    
    int pipefd[2];
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(1);
    }
    
    int status = 0;
    int pid = fork();
    
    
    switch(pid){
        
        case -1 :
            perror("fork failed");
            return -1;
        
        case 0 :
            
            close(STDOUT);
            dup(pipefd[WRITE_END]);
            close(pipefd[WRITE_END]);
            
            if( line->output_redirect ){
                int fd = open(line->output_redirect,O_RDWR | O_CREAT, 0666);
                close(STDOUT);
                dup(fd);
                close(fd);
            }
            
            if( line->input_redirect ){
                int fd = open(line->input_redirect,O_RDONLY);
                close(STDIN);
                dup(fd);
                close(fd);
            }
            
            execvp(line->arguments[0],line->arguments);
            
            break;
        
        default :
            close(pipefd[WRITE_END]);
            int cpid = fork();
            switch(cpid){
                
                case -1 :
                    perror("fork failed");
                    break;
                
                case 0 :
                    close(STDIN);
                    dup(pipefd[READ_END]);
                    close(pipefd[READ_END]);
                    
                    if( other_command->output_redirect ){
                        int fd = open( other_command->output_redirect,O_RDWR | O_CREAT, 0666);
                        close(STDOUT);
                        dup(fd);
                        close(fd);
                    }
            
                    if(  other_command->input_redirect ){
                        int fd = open( other_command->input_redirect,O_RDONLY);
                        close(STDIN);
                        dup(fd);
                        close(fd);
                    }
                    if( execvp( other_command->arguments[0],  other_command->arguments ) == -1){
                        _exit(0);
                    }
                    break;
                
                default :
                    close(pipefd[READ_END]);
                    if( other_command->blocking ) waitpid(cpid, &status, 0);
                    break;
                
                
            }
            if( line->blocking ) waitpid(pid, &status, 0);
            break;
        
    }
    return 0;
}



 int execute_regular(cmd_line *line){
     
    
    int status = 0;
    int pid = fork();
    
    
    switch(pid){
        
        case -1 :
            perror("fork failed");
            break;
        
        case 0 :
            
            if( line->output_redirect ){
                int fd = open(line->output_redirect,O_RDWR | O_CREAT, 0666);
                close(STDOUT);
                dup(fd);
            }
            
            if( line->input_redirect ){
                int fd = open(line->input_redirect,O_RDONLY);
                close(STDIN);
                dup(fd);
            }
            if( execvp(line->arguments[0], line->arguments ) == -1){
                _exit(0);
            }
            break;
        
        default :
            
            if( line->blocking ) waitpid(pid, &status, 0);
            break;
        
        
    }
    return 0;
}
 
 

int simple_strcmp(const char *str1, const char *str2) {
    while (*str1 == *str2 && *str1) {
        str1++;
        str2++;
    }
    return *str1 - *str2;
}

 
 
 
